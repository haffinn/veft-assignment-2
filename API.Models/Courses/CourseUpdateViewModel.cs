﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Models.Courses
{
    /// <summary>
    /// This class is used when a course is being updated
    /// </summary>
    public class CourseUpdateViewModel
    {
        /// <summary>
        /// The start date of the course.
        /// Format: MM-dd-yyy
        /// Example: "08-11-2015"
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }
        
        /// <summary>
        /// The end date of the course.
        /// Format: MM-dd-yyy
        /// Example: "11-17-2015"
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }

    }
}
