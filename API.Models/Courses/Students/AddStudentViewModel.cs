﻿using System.ComponentModel.DataAnnotations;

namespace API.Models.Courses.Students
{
    /// <summary>
    /// This class represents a single student in a course, when a student is added to a course
    /// </summary>
    public class AddStudentViewModel
    {
        /// <summary>
        /// The social security number of the student.
        /// Example: "0101852469"
        /// </summary>
        [Required]    
        public string SSN { get; set; }
    }
}
