﻿namespace API.Models.Courses.Students
{
    /// <summary>
    /// This class represents a single student which can be registered in courses.
    /// </summary>
    public class StudentDTO
    {
        /// <summary>
        /// The social security number of the student.
        /// Example: "0101852469"
        /// </summary>
        public string SSN { get; set; }
        /// <summary>
        /// Full name of the student.
        /// Example: "Hafþór Snær Þórsson"
        /// </summary>
        public string Name { get; set; }
    }
}
