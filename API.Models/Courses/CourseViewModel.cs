﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Models.Courses
{
    /// <summary>
    /// This class represents the data necessary to create a single course,
    /// taught in a given semester. 
    /// </summary>
    public class CourseViewModel
    {
        /// <summary>
        /// The Template ID of the course template being crated.
        /// Example: "T-514-VEFT"
        /// </summary>
        [Required]
        public string TemplateID { get; set; }

        /// <summary>
        /// Start date of the course.
        /// Exmaple: "2015-08-15"
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End date of the course.
        /// Format: MM-dd-yyy
        /// Example: "08-11-2015"
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The semsester in which the course is taught.
        /// Format: MM-dd-yyy
        /// Example: "11-17-2015"
        /// </summary>
        [Required]
        public string Semester { get; set; }
    }
}
