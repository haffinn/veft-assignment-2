﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Models.Courses;
using API.Models.Courses.Students;
using API.Services.Entities;
using API.Services.Exceptions;
using API.Services.Repositories;

namespace API.Services.ServiceProviders
{
    public class CoursesServiceProvider
    {
        private readonly AppDataContext _db;

        public CoursesServiceProvider()
        {
            _db = new AppDataContext();
        }

        /// <summary>
        /// Returns a list of courses belonging to a given semester.
        /// If no semester is provided, the "current" semester will be used.
        /// </summary>
        /// <param name="semester">The given semester</param>
        /// <returns>List of courses</returns>
        public List<CourseDTO> GetCoursesBySemester(string semester = null)
        {
            //1. Validate

            // If semester is not specified - the current semester is used.
            // As of now, the value of current semester is hardcoded for Fall 2015.
            if (string.IsNullOrEmpty(semester))
            {
                semester = "20153";
            }

            //2. Get Courses

            //get student count for each course
            var count =
                from cs in _db.CourseStudents
                group cs by cs.CourseID into grp
                select new
                {
                    ID = grp.Key,
                    studCount = grp.Count()
                };

            var res =
                (from c in _db.Courses
                from ct in _db.CourseTemplates.Where(x => x.TemplateID == c.TemplateID)
                from cs in count.Where(x => x.ID == c.ID)
                where c.Semester == semester
                select new CourseDTO
                {
                    ID = c.ID,
                    Name = ct.Name,
                    StartDate = c.StartDate,
                    StudentCount = cs.studCount
                }).ToList();

            //3. Return
            return res;
        }

        public CourseDetailsDTO GetCourseByID(int id)
        {
            //1. Validate
            var isAvailable = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (isAvailable == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Get and assemble the course details
            var course =
               (from c in _db.Courses
                from ct in _db.CourseTemplates.Where(x => x.TemplateID == c.TemplateID)
                where c.ID == id
                select new CourseDetailsDTO
                {
                    ID = c.ID,
                    Name = ct.Name,
                    TemplateID = ct.TemplateID,
                    Semester = c.Semester,
                    StartDate = c.StartDate,
                    EndDate = c.EndDate
                }).SingleOrDefault();
            var students = GetStudentsInCourse(id);
            if (students != null)
            {
                if (course != null) course.Students = students;
            }


            //3. Return
            return course;
        }

        public CourseDetailsDTO AddNewCourse(CourseViewModel model)
        {
            //1. Validate
            var templateIsAvailable = _db.CourseTemplates.SingleOrDefault
                (x => x.TemplateID == model.TemplateID);
            if (templateIsAvailable == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Add the new course to the database
            var newCourse = new Courses
            {
                TemplateID = model.TemplateID,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Semester = model.Semester
            };

            _db.Courses.Add(newCourse);
            _db.SaveChanges();

            int newID = newCourse.ID;

            //3. Return
            var myCourse = GetCourseByID(newID);

            return myCourse;
        }

        public int DeleteCourse(int id)
        {
            //1. Validate

            var course = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (course == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Delte course
            _db.Courses.Remove(course);

            // Delete all student registrastion info
            var students = _db.CourseStudents.Where(x => x.CourseID == id).ToList();
            foreach (var student in students)
            {
                _db.CourseStudents.Remove(student);
            }
            _db.SaveChanges();

            //3. Return
            return 1;

        }

        public List<StudentDTO> GetStudentsInCourse(int id)
        {
            //1. Validate
            var isAvailable = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (isAvailable == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Get students
            var students =
                (from p in _db.Persons
                from cs in _db.CourseStudents.Where(x => x.PersonID == p.ID)
                where cs.CourseID == id
                select new StudentDTO
                {
                    Name = p.Name,
                    SSN = p.SSN
                }).ToList();

            //3. Return
            return students;

        }

        public StudentDTO AddStudentToCourse(int id, AddStudentViewModel model)
        {
            //1. Validation
            var course = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (course == null)
            {
                throw new AppObjectNotFoundException();
            }
            
            var person = _db.Persons.SingleOrDefault(x => x.SSN == model.SSN);
            if (person == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Add the student to the course
            var courseStudent = new CourseStudents
            {
                PersonID = person.ID,
                CourseID = course.ID
            };

            _db.CourseStudents.Add(courseStudent);
            _db.SaveChanges();

            //3. Figure out what to return

            var returnValue = new StudentDTO
            {
                Name = person.Name,
                SSN = person.SSN
            };


            return returnValue;
        }

        public CourseDTO UpdateCourse(int id, CourseUpdateViewModel model)
        {
            //1. Validate

            var courseEntity = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (courseEntity == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Update

            courseEntity.StartDate = model.StartDate;
            courseEntity.EndDate = model.EndDate;
            _db.SaveChanges();

            //3. Return
            var courseTemplate = _db.CourseTemplates.SingleOrDefault(x => x.TemplateID == courseEntity.TemplateID);
            if (courseTemplate == null)
            {
                throw new AppInternalServerErrorException();
            }

            var count = _db.CourseStudents.Count(x => x.CourseID == courseEntity.ID);
            var result = new CourseDTO
            {
                ID = courseEntity.ID,
                Name = courseTemplate.Name,
                StartDate = model.StartDate,
                StudentCount = count
            };
            return result;
        }
    }
}
