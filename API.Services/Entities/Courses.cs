﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Services.Entities
{
    /// <summary>
    /// This private class represents database entries for Courses Table
    /// </summary>
    [Table("Courses")]
    class Courses
    {
        /// <summary>
        /// Database-generated identification, unique
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Code for template which the course is based on.
        /// Example: "T-514-VEFT"
        /// </summary>
        public string TemplateID { get; set; }

        /// <summary>
        /// Start date of the course.
        /// Example: "2015-08-15"
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Date for when the course is closed.
        /// Example: "2015-11-18"
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Semester in which the course is taught.
        /// Example: "20153"
        /// </summary>
        public string Semester { get; set; }
    }
}
