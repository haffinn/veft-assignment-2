﻿
namespace API.Services.Entities
{
    /// <summary>
    /// This class represents the database table for students in course.
    /// </summary>
    class CourseStudents
    {
        /// <summary>
        /// A database-generated identification for a single connection.
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// Database ID of the course which the student is registered in
        /// </summary>
        public int CourseID { get; set; }
        
        /// <summary>
        /// Database ID of the student registered in a course
        /// </summary>
        public int PersonID { get; set; }
    }
}
